var Queue = {
  'errored_collection': 'errored',
  'assigned_collection': 'assigned',
  'unassigned_collection': 'unassigned'
}

function Task(queue, data, ttl, priority) {
  this.queue = queue;
  this.data = data;

  this.ttl = (ttl == undefined ? 60 : ttl);
  this.priority = (priority == undefined ? 0 : priority);
  this.created = null;
  this.ttl = null;
  this.assigned_to = null;
}

Task.prototype.to_dict = function() {
  if (this.created == null) {
    var date = new Date();
    var now_utc =  Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
    date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
    this.created = new Date(now_utc);
  }

  task_dict = {
    'created': this.created,
    'data': this.data,
    'ttl': this.ttl,
    'assigned_to': null,
    'priority': this.priority
  }

  if (this.assigned_to != null) {
    task_dict['assigned_to'] = this.assigned_to
  }

  return task_dict
}

Task.prototype.create = function(db) {
  console.log(this);
  console.log(Queue.unassigned_collection)
  this.ref = db.collection('queues').doc(this.queue).collection(Queue.unassigned_collection).doc();
  this.id = this.ref.id
  this.ref.set(this.to_dict());
}

module.exports = {
  Task,
  Queue
}