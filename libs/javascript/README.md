# Firestore MQ
Install `npm install --save firestoremq`

## TODO
- Add JavaScript client

## Examples
### Add a task
```JavaScript
var firestoreMQ = require('firestoreMQ');
var admin = require('firebase-admin');

// TODO
admin.initializeApp({
  credential: admin.credential.cert(service_acount_path),
  databaseURL: `https://${project_id}.firebaseio.com`
});

var firestore = admin.firestore();
firestore.settings({timestampsInSnapshots: true});

// Add a task
var queue = 'process_images';
var data = {
  "image_path": "/path/to/images.png"
}
var ttl = 60 // seconds (optional)
var priority = 0 // Higher priroty is higher number  (optional)

var task = new firestoreMQ.Task(queue, data, ttl, priority);
task.create(firestore)
```