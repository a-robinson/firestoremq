#!/usr/bin/env python3

import firestoreMQ
import time
import argparse
import sys
import firebase_admin
from firebase_admin import firestore, credentials

parser = argparse.ArgumentParser(description='Worker')
parser.add_argument('config', help='Firebase service account')
parser.add_argument('worker_id', help='Worker ID')
parser.add_argument('queue', help='Queue ID')

args = parser.parse_args()

cred = credentials.Certificate(args.config)
firebase_admin.initialize_app(cred)
db = firestore.client()

print('[Worker %s] Watching queue %s' % (args.worker_id, args.queue))
while True:
  task = firestoreMQ.next_task(db, args.queue, args.worker_id) # Blocking call for next task

  print('Task %s' % task.id)
  print(task.data)
  time.sleep(2)
  task.complete(db)