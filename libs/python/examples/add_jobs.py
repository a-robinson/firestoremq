#!/usr/bin/env python3

import firestoreMQ
import time
import argparse
import sys
import firebase_admin
from firebase_admin import firestore, credentials

parser = argparse.ArgumentParser(description='Worker')
parser.add_argument('config', help='Firebase service account')
parser.add_argument('queue', help='Queue ID')
parser.add_argument('job_count', type=int, help='Number of jobs to create')

args = parser.parse_args()

cred = credentials.Certificate(args.config)
firebase_admin.initialize_app(cred)
db = firestore.client()

print('Adding %i jobs to queue %s' % (args.job_count, args.queue))
for i in range(args.job_count):
    data = {
      "number": i
    }
    ttl = 30 # seconds (optional)
    priority = 0 # higher number if higher priorty (optional)

    task = firestoreMQ.Task(args.queue, data, ttl=ttl, priority=priority)
    task.create(db)
    print('Added task %s' % task.id)