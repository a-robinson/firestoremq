#!/usr/bin/env python3

import argparse
import sys
import firebase_admin
from firebase_admin import firestore, credentials
import firestoreMQ
import json

def cmd_status(args):
    if args.queue:
        firestoreMQ.Queue(args.queue).status(db)
    else:
        queues_docs = db.collection('queues').get()
        queues = list(queues_docs)
        print('%i queue(s)' % len(queues))
        for queue_doc in queues:
            print('-- %s' % queue_doc.id)
            firestoreMQ.Queue(queue_doc.id).status(db)
            print()

def cmd_add(args):
    data = json.loads(args.data)
    task = firestoreMQ.Task(args.queue, data, ttl=args.ttl, priority=args.priority)
    task.create(db)
    print('Added task %s' % task.id)

def cmd_clear(args):
    firestoreMQ.Queue(args.queue).delete(db)

def cmd_dump(args):
    print(firestoreMQ.Queue(args.queue).dump(db))

parser = argparse.ArgumentParser(description='Interact with firestore MQ')
parser.add_argument('--config', required=True, help='Firebase service account')
subparsers = parser.add_subparsers(help='Sub commands', dest='command')

parser_status = subparsers.add_parser('status', help='Display queue status')
parser_status.add_argument('--queue', help='Queue to show status of')

parser_add = subparsers.add_parser('add', help='Add queue status')
parser_add.add_argument('queue', help='Queue to add task to')
parser_add.add_argument('data', help='Task JSON data')
parser_add.add_argument('--ttl', type=int, default=60, help='Time to live in seconds')
parser_add.add_argument('--priority', type=int, default=0, help='Task priority, higher is higher priority')

parser_clear = subparsers.add_parser('clear', help='Clear queue tasks deleting all')
parser_clear.add_argument('queue', help='Queue to clear')

parser_dump = subparsers.add_parser('dump', help='Dump queue tasks in JSON')
parser_dump.add_argument('queue', help='Queue to dump')


args = parser.parse_args()

cred = credentials.Certificate(args.config)
firebase_admin.initialize_app(cred)
db = firestore.client()

if args.command == 'status':
  cmd_status(args)
elif args.command == 'add':
  cmd_add(args)
elif args.command == 'clear':
  cmd_clear(args)
elif args.command == 'dump':
  cmd_dump(args)
else:
    parser.print_help()